# -*- coding: utf-8 -*-

import flask

OTHER_SOFTWARE = {
    'Flask': {
        'id': 'Q289281',
        'description': 'a Python web application framework',
        'license': {
            'name': 'three-clause BSD license',
            'url': 'http://flask.pocoo.org/docs/0.12/license/#flask-license',
        },
    },
    'Bootstrap': {
        'id': 'Q893195',
        'description': ('a web front-end framework '
                        '(we only use the '
                        '<abbr title="Cascading Style Sheets">'
                        'CSS</abbr> part)'),
        'license': {
            'name': 'MIT License',
            'url': 'https://github.com/twbs/bootstrap/blob/master/LICENSE',
        },
    },
    'Python': {
        'id': 'Q28865',
        'description': 'a dynamic programming language',
        'license': {
            'name': 'Python Software Foundation License',
            'url': ('https://docs.python.org/3/license.html#'
                    'psf-license-agreement-for-python-release'),
        },
    },
    'Requests': {
        'id': 'Q22661317',
        'description': 'a Python web request library',
        'license': {
            'name': 'Apache2 License',
            'url': ('http://docs.python-requests.org/en/master/user/intro/#'
                    'requests-license'),
        },
    },
    'RDFLib': {
        'id': 'Q7276224',
        'description': ('a Python '
                        '<abbr title="Resource Description Framework">'
                        'RDF</abbr> library'),
        'license': {
            'name': 'three-clause BSD license',
            'url': 'https://github.com/RDFLib/rdflib/blob/master/LICENSE',
        },
    },
    'Redis-py': {
        'id': 'Q51471759',
        'description': ('a Python library for interacting '
                        'with the Redis key-value store'),
        'license': {
            'name': 'MIT License',
            'url': ('https://github.com/andymccurdy/redis-py'
                    '/blob/master/LICENSE'),
        },
    },
    'PyYAML': {
        'id': 'Q10852208',
        'description': ('a Python library for parsing '
                        '<abbr title="YAML Ain’t Markup Language">'
                        'YAML</abbr> files'),
        'license': {
            'name': 'MIT License',
            'url': 'https://github.com/yaml/pyyaml/blob/master/LICENSE',
        },
    },
    'toolforge': {
        'id': 'Q51474059',
        'description': ('a Python library with useful functions '
                        'for Toolforge tools'),
        'license': {
            'name': 'GNU General Public License, version 3',
            'url': 'https://github.com/legoktm/toolforge/blob/master/COPYING',
        },
    },
}


def licensePage():
    with open('LICENSE', 'r') as license:
        return flask.render_template(
            'license.html',
            license=license.read(),
            otherSoftware=OTHER_SOFTWARE,
        )
