# -*- coding: utf-8 -*-

# WARNING! These lists contain titles in right-to-left scripts,
# and a compliant viewer will display them right-to-left,
# *including intermediate punctuation*: the codepoint sequence
#     'foo.org|AAA:BBB': 'CCC:DDD/EEE'
# can look like
#     'foo.org|EEE/DDD:CCC' :'BBB:AAA'
# – check the alignment of the colon in the middle
# if you’re not sure how your viewer handles this.

# flake8: noqa
# (we only need to disable E501 line too long,
# but flake8 doesn’t support disabling individual errors for entire files)

# Some wikis don’t want MassMessages to go to their regular project chat,
# or have their project chat split up by date or some other criteria.
# Replace pages in the dict key (wiki|title) with the dict value instead.
page_overrides = {
    'an.wikipedia.org|Wikipedia:Tabierna': 'Wikipedia:Tabierna/Noticias',
    "as.wikipedia.org|ৱিকিপিডিয়া:ৰাইজৰ চ'ৰা": "ৱিকিপিডিয়া:ৰাইজৰ চ'ৰা (বিবিধ)",
    'azb.wikipedia.org|ویکی‌پدیا:کند مئیدانی': 'ویکی‌پدیا:کند مئیدانی/اوتوماتیک پیغاملر',
    'bg.wikinews.org|Уикиновини:Разговори': 'Уикиновини:Новини за редактори',
    'bg.wikipedia.org|Уикипедия:Разговори': 'Уикипедия:Новини',
    'br.wikipedia.org|Wikipedia:Kumuniezh/An davarn': 'Wikipedia:Kumuniezh/An davarn/sizhunvezh {{#time:W|{{CURRENTTIMESTAMP}}}}, bloaz {{#time:Y|{{CURRENTTIMESTAMP}}|br}}',
    'br.wikisource.org|Wikimammenn:An davarn': 'Wikimammenn:An davarn/Miz {{ucfirst:{{#time:F Y|{{CURRENTTIMESTAMP}}|br}}}}',
    'bs.wikipedia.org|Wikipedia:Čaršija/Tehnika': 'Wikipedia:Čaršija/Tehnika/Vijesti',
    'ca.wikipedia.org|Viquipèdia:La taverna': 'Viquipèdia:La taverna/Novetats',
    'ckb.wikipedia.org|ویکیپیدیا:دیوەخان': 'ویکیپیدیا:دیوەخان (جۆراوجۆر)',
    'en.wikibooks.org|Wikibooks:Reading room': 'Wikibooks:Reading room/General',
    'en.wikinews.org|Wikinews:Water cooler': 'Wikipedia:Water cooler/miscellaneous',
    'en.wikipedia.org|Wikipedia:Village pump': 'Wikipedia:Village pump (miscellaneous)',
    'en.wiktionary.org|Wiktionary:Beer parlour': 'Wiktionary:Beer parlour/{{CURRENTYEAR}}/{{CURRENTMONTHNAME}}',
    'en.wiktionary.org|Wiktionary:Grease pit': 'Wiktionary:Grease pit/{{CURRENTYEAR}}/{{CURRENTMONTHNAME}}',
    'eo.wikipedia.org|Vikipedio:Diskutejo': 'Vikipedio:Diskutejo/Diversejo',
    'es.wikipedia.org|Wikipedia:Café': 'Wikipedia:Café/Archivo/Noticias/Actual',
    'et.wikisource.org|Vikitekstid:Üldine arutelu': 'Vikitekstid:Üldine arutelu/Saatkond',
    'fa.wikipedia.org|ویکی‌پدیا:قهوه‌خانه': 'ویکی‌پدیا:قهوه‌خانه/گوناگون',
    'fr.wikibooks.org|Wikilivres:Le Bistro': 'Discussion Wikilivres:Le Bistro/{{CURRENTYEAR}}',
    'fr.wikinews.org|Wikinews:Salle café': 'Wikinews:Salle café/{{#time:Y/F|{{CURRENTTIMESTAMP}}|fr}}',
    'fr.wikipedia.org|Wikipédia:Le Bistro': 'Wikipédia:Le Bistro/{{#time:j F Y|+10 hours|fr}}',
    'fr.wikiquote.org|Wikiquote:Le Salon': 'Wikiquote:Le Salon/{{#time:F Y|{{CURRENTTIMESTAMP}}|fr}}',
    'fr.wikisource.org|Wikisource:Scriptorium': 'Wikisource:Scriptorium/{{ucfirst:{{#time:F Y|{{CURRENTTIMESTAMP}}|fr}}}}',
    'fr.wikiversity.org|Wikiversité:La salle café': 'Wikiversité:La salle café/{{#time:F|{{CURRENTMONTHNAME}}|fr}} {{CURRENTYEAR}}',
    'fr.wiktionary.org|Wiktionnaire:Wikidémie': 'Wiktionnaire:Wikidémie/{{#time:F Y|{{CURRENTTIMESTAMP}}|fr}}',
    'fr.wiktionary.org|Wiktionnaire:Questions techniques': 'Wiktionnaire:Questions techniques/{{#time:F Y|{{CURRENTTIMESTAMP}}|fr}}',
    'fi.wikinews.org|Wikiuutiset:Kahvihuone': 'Wikiuutiset:Kahvihuone (uutiset)',
    'fi.wikipedia.org|Wikipedia:Kahvihuone': 'Wikipedia:Kahvihuone (uutiset)',
    'gl.wikipedia.org|Wikipedia:A Taberna': 'Wikipedia:A Taberna (novas)',
    'hr.wikipedia.org|Wikipedija:Kafić': 'Wikipedija:Kafić/Foreign languages',
    'hr.wikiquote.org|Wikicitat:Pisarnica': 'Wikicitat:Pisarnica/Foreign languages',
    'hr.wikisource.org|Wikizvor:Pisarnica': 'Wikizvor:Pisarnica/Foreign languages',
    # 'hu.wikibooks.org|Wikikönyvek:Társalgó': 'Wikikönyvek/Társalgó', # this page is listed on meta:Distribution list/Global message delivery, but now redirects to the proper (namespaced) one
    'hu.wikipedia.org|Wikipédia:Kocsmafal': 'Wikipédia:Kocsmafal (egyéb)',
    'id.wikipédia.org|Wikipedia:Warung Kopi': 'Wikipedia:Warung Kopi (Kebijakan)',
    'ilo.wikipedia.org|Wikipedia:Dap-ayan': 'Wikipedia:Dap-ayan/Nadumaduma {{#time:Y|{{CURRENTTIMESTAMP}}|ilo}}',
    'ilo.wikipedia.org|Wikipedia:Dap-ayan/Teknikal': 'Wikipedia:Dap-ayan/Teknikal/Damdamag {{#time:Y-F|{{CURRENTTIMESTAMP}}|ilo}}',
    'ka.wikipedia.org|ვიკიპედია:ფორუმი': 'ვიკიპედია:ფორუმი/მრავალენოვანი',
    'kab.wikipedia.org|Wikipedia:Tajmaɛt': 'Wikipedia:Tajmaɛt/{{#time:F Y|{{CURRENTTIMESTAMP}}|kab}}',
    'kk.wikipedia.org|Уикипедия:Форум': 'Уикипедия:Форум/Жаңалықтар',
    'ko.wikipedia.org|위키백과:사랑방': '위키백과:사랑방/{{#time:o|+9 hours}}년 제{{#expr:{{#time:W|+9 hours}}}}주',
    'ko.wikipedia.org|위키백과:사랑방 (기술)': '위키백과:사랑방 (기술)/{{#time:Y년 n월|+9 hours}}',
    'ko.wiktionary.org|위키낱말사전:자유게시판': '위키낱말사전:자유게시판/{{#time:o|+9 hours}}년',
    'ku.wikipedia.org|Wîkîpediya:Dîwan': 'Wîkîpediya:Dîwan (peyamên MediaWiki)',
    'lrc.wikipedia.org|ڤیکیپئدیا:چاست حوٙنە': 'ڤیکیپئدیا:چاست حوٙنە/جوٙر ڤا جوٙر',
    'mn.wikipedia.org|Wikipedia:Хурал': 'Wikipedia:Хурал/Гадаад хэл',
    'mr.wikipedia.org|विकिपीडिया:चावडी/प्रगती': 'विकिपीडिआ:चावडी/प्रगती',
    'ms.wikipedia.org|Wikipedia:Kedai Kopi': 'Wikipedia:Kedai Kopi (lain-lain)',
    'nl.wikiquote.org|Wikiquote:De kantine': 'Wikiquote:De kantine/archief Wikimedia-meldingen',
    'pl.wikinews.org|Wikinews:Kawiarenka': 'Wikinews:Kawiarenka/Masowe wiadomości',
    'pl.wikipedia.org|Wikipedia:Kawiarenka pod Wesołym Encyklopedystą': 'Wikipedia:Kawiarenka/Ogólne',
    'pl.wikisource.org|Wikiźródła:Skryptorium': 'Wikiźródła:Skryptorium/Komunikaty',
    'pl.wiktionary.org|Wikisłownik:Bar': 'Wikisłownik:Bar/Społeczność Wikisłownika',
    'pt.wikipedia.org|Wikipédia:Esplanada': 'Wikipédia:Esplanada/anúncios',
    'ru.wikinews.org|Викиновости:Форум/Общий': 'Викиновости:Форум/Иноязычный',
    'ru.wikipedia.org|Википедия:Форум': 'Википедия:Форум/Новости',
    'si.wikipedia.org|විකිපීඩියා:කෝපි කඩේ': 'විකිපීඩියා:කෝපි කඩේ (miscellaneous)',
    'sk.wikipedia.org|Wikipédia:Krčma': 'Wikipédia:Krčma/Novinky',
    'sr.wikipedia.org|Википедија:Трг': 'Википедија:Трг/Енглески',
    'su.wikipedia.org|Wikipedia:Sawala': 'Wikipedia:Sawala (kawijakan)',
    'sv.wikipedia.org|Wikipedia:Bybrunnen': 'Wikipedia:Bybrunnen/Massmeddelanden',
    'szl.wikipedia.org|Wikipedyjo:Sztamtisz': 'Wikipedyjo:Sztamtisz/Uogůlne',
    'ta.wikipedia.org|விக்கிப்பீடியா:ஆலமரத்தடி': 'விக்கிப்பீடியா:ஆலமரத்தடி_(அறிவிப்புகள்)',
    'tg.wikipedia.org|Википедиа:Қаҳвахона': 'Википедиа:Қаҳвахона/Умумӣ',
    'tr.wikipedia.org|Vikipedi:Köy çeşmesi': 'Vikipedi:Köy çeşmesi (ilginize)',
    'uk.wikipedia.org|Вікіпедія:Кнайпа': 'Вікіпедія:Кнайпа (різне)',
    'ur.wikipedia.org|ویکیپیڈیا:دیوان عام': 'منصوبہ:دیوان عام/متفرقات',
    'wuu.wikipedia.org|Wikipedia:红茶坊': 'Wikipedia:沙盘',
    'www.wikidata.org|Wikidata:Contact the development team': 'Wikidata:Project chat',
    'zh.wikipedia.org|Wikipedia:互助客栈': 'Wikipedia:互助客栈/消息',
    'zh-yue.wikipedia.org|Wikipedia:城市論壇': 'Wikipedia:城市論壇 (技術)',
}

# Some wikis would like to receive MassMessages more than once.
# If the target contains pages in the dict key (wiki|title),
# add the elements of the dict value as well.
extra_pages = {
    'commons.wikimedia.org|Commons:Village pump': [
        'Commons:Village pump/zh',
        'Commons:Thảo luận',
    ],
}

# Some wikis don’t have a generic page that could be linked to Wikidata at all,
# but would still like to receive messages.
# If a message is to be sent to the dict key (wiki|item ID),
# send it to the dict value.
unlinked_pages = {
    # NOTE: wdmm.getSitelinksForPage assumes the second part of the domain is the wikigroup
    'lt.wikiquote.org|Q16503': 'Vikicitatos Aptarimas:Atstovybė (komentaras)',
    'ml.wiktionary.org|Q16503': 'വിക്കിനിഘണ്ടു:വിക്കി പഞ്ചായത്ത് (വാര്‍ത്തകള്‍)',
    'vi.wikibooks.org|Q16503': 'Wikibooks:Bàn thảo luận/Bảng thông tin',
}
